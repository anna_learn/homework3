﻿using HomeWork3;
using System.Collections;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;

bool exitFlag = false;
int cursorPosition = 0;
string[] abc = new string[3] { string.Empty, string.Empty,string.Empty};
StringBuilder sb = new StringBuilder();
int a = 0;
int b = 0;
int c = 0;
var dict = new Dictionary<string, string>();

while (!exitFlag)
{
    dict["a"] = abc[0];
    dict["b"] = abc[1];
    dict["c"] = abc[2];
    DrawMenu(cursorPosition, dict);
    try
    {
        if (GetParams(dict, out a, out b, out c))
        {
            DrawResult(SoluteSquareEquation(a, b, c));
        }
    }

    catch (InvalidCalcException ice)
    {
        FormatData(ice.Message, Severity.Warning, dict);
    }
    catch (FormatException fe)
    {
        FormatData(fe.Message, Severity.Error, dict);
    }
    catch (OverflowException oe)
    {
        OverFlowMessage(oe.Message);
    }
    Console.SetCursorPosition(4 + dict.Values.ElementAt(cursorPosition).Length, cursorPosition + 1);
    var keyValue = Console.ReadKey();
    switch (keyValue.Key)
    {
        case ConsoleKey.UpArrow:
            if (cursorPosition > 0)
                cursorPosition--;
            break;
        case ConsoleKey.DownArrow:
            if (cursorPosition < abc.Length - 1)
                cursorPosition++;
            break;
        case ConsoleKey.Backspace:
            if (abc[cursorPosition].Length > 0)
            {
                abc[cursorPosition] = abc[cursorPosition].Remove(abc[cursorPosition].Length - 1);

            }
            break;

        default:
            sb.Clear().Append(abc[cursorPosition]).Append(keyValue.KeyChar);
            abc[cursorPosition] = sb.ToString();

            break;

    }
}
static void FormatData(string message, Severity severity, IDictionary data)
{
    Console.SetCursorPosition(0, 6);
    var background = Console.BackgroundColor;
    var foreground= Console.ForegroundColor;
    if (severity == Severity.Error)
    {
        Console.BackgroundColor = ConsoleColor.Red;
        Console.ForegroundColor = ConsoleColor.White;     
    }
    else
    {
        Console.BackgroundColor = ConsoleColor.Yellow;
        Console.ForegroundColor = ConsoleColor.Black;
    }
    Console.WriteLine("--------------------------------------------------");
    Console.WriteLine(message);
    Console.WriteLine("--------------------------------------------------");
    foreach (var val in data as Dictionary<string,string>) 
    {
        Console.WriteLine($"{val.Key} = {val.Value}");   
    
    }
    Console.BackgroundColor = background;
    Console.ForegroundColor = foreground;
}
static double[] SoluteSquareEquation (int a, int b, int c)
{
    double dis=b*b - 4* a * c;
    if (dis < 0)
    {
        throw new InvalidCalcException("Вещественных значений не найдено");
    }
    else if (dis == 0)
    {
        return new double[1] { -b / 2 * a };
    }
    else
    {
        return new double[2] { (-b + Math.Sqrt(dis)) / 2 * a, (-b - Math.Sqrt(dis)) / 2 * a };
    }
}
static void DrawMenu (int cursorPosition, Dictionary<string, string> values)
{
    Console.Clear();
    Console.SetCursorPosition(0, 0);
    StringBuilder _sb = new StringBuilder();
    _sb.Append(string.IsNullOrEmpty(values["a"]) ? "a" : values["a"]).Append("*x^2");
    if (string.IsNullOrEmpty(values["b"]))
        _sb.Append("+b"); 
    else if (values["b"][0]!='-')
        _sb.Append("+");

    _sb.Append(values["b"]).Append("*x");
      if (string.IsNullOrEmpty(values["c"]))
        _sb.Append("+c");
    else if (values["c"][0] != '-')
        _sb.Append("+");

    _sb.Append(values["c"]).Append("=0");
        
    Console.WriteLine(_sb.ToString());
    for (int i = 0; i < values.Count; i++)
    {
        _sb.Clear();
        _sb.Append(i == cursorPosition ? ">" : " ").Append(values.Keys.ElementAt(i)).Append(": ").Append(values.Values.ElementAt(i));
        Console.WriteLine(_sb.ToString());

    }
    
}
static void DrawResult(double[] values) 
{
    Console.SetCursorPosition(0, 4);
    if (values.Length == 1)
    {
        Console.WriteLine($"x = {values[0]}");
    }
    else
    {
        Console.WriteLine($"x1 = {values[0]}, x2 = {values[1]}");
    }
}
static bool GetParams(Dictionary<string, string> values, out int a, out int b, out int c)
{
    a = b = c = 0;
    if (values.Values.Contains(string.Empty))
    {
        return false;
    }
    else
    {
        foreach (var value in values)
        {
            int currentValue;
            try
            {
                currentValue = int.Parse(value.Value);
            }
            catch (FormatException)
            {
                throw new FormatException($"Неверное значение параметра {value.Key}");
            }
            catch (OverflowException)
            {
                throw new OverflowException($"Параметр {value.Key} должен быть в диапазоне от {int.MinValue} до {int.MaxValue}");
            }
            if (value.Key == "a")
                a = currentValue;
            else if (value.Key == "b")
                b = currentValue;
            else c = currentValue;
        }

        return true;
    }
}
static void OverFlowMessage(string message)
{
    Console.SetCursorPosition(0, 5);
    var background = Console.BackgroundColor;
    var foreground = Console.ForegroundColor;

    Console.BackgroundColor = ConsoleColor.Green;
    Console.ForegroundColor = ConsoleColor.White;
    Console.WriteLine(message);
    Console.BackgroundColor = background;
    Console.ForegroundColor = foreground;
}
enum Severity
{
    Warning,
    Error
}